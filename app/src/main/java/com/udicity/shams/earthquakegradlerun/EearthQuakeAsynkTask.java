package com.udicity.shams.earthquakegradlerun;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import java.util.List;

/**
 * Created by Shady_Keshk_BioLab on 02/08/17.
 */

public class EearthQuakeAsynkTask extends AsyncTaskLoader<List<EarthQuake>> {

    private static final String LOG_TAG = EearthQuakeAsynkTask.class.getName();

    /** Query URL */
    private String mUrl;

    /**
     * Constructs a new {@link EearthQuakeAsynkTask}.
     *
     * @param context of the activity
     * @param url to load data from
     */
    public EearthQuakeAsynkTask(Context context, String url) {
        super(context);
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        Log.v(LOG_TAG,"on start Loading");
        forceLoad();
    }

    @Override
    public List<EarthQuake> loadInBackground() {
        Log.v(LOG_TAG,"on start in Background");

        if (mUrl == null) {
            return null;
        }

        List<EarthQuake> result = QueryUtils.fetchEarthQuakeData(mUrl);
        return result;
    }
}
