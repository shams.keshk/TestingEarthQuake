package com.udicity.shams.earthquakegradlerun;

/**
 * Created by shams on 09/07/17.
 */

public class EarthQuake {

    private double magnitude;
    private String place;
    private Long date;
    private String url;

    public EarthQuake(double magnitude, String place, Long date ,String url) {
        this.magnitude = magnitude;
        this.place = place;
        this.date = date;
        this.url = url;
    }

    public double getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(double magnitude) {
        this.magnitude = magnitude;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
