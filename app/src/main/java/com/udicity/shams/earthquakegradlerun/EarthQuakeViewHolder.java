package com.udicity.shams.earthquakegradlerun;

import android.view.View;
import android.widget.TextView;

/**
 * Created by shams on 09/07/17.
 */

public class EarthQuakeViewHolder {

    private TextView magnitude;
    private TextView primary_location;
    private TextView location_offset;
    private TextView date;
    private TextView time;

    public EarthQuakeViewHolder(View view) {
        this.magnitude = (TextView) view.findViewById(R.id.magnitude);
        this.primary_location = (TextView) view.findViewById(R.id.primary_location);
        this.location_offset = (TextView) view.findViewById(R.id.location_offset);
        this.date = (TextView) view.findViewById(R.id.date_id);
        this.time = (TextView) view.findViewById(R.id.time_id);
    }

    public TextView getMagnitude() {
        return magnitude;
    }


    public TextView getPrimary_location() {
        return primary_location;
    }

    public TextView getLocation_offset() {
        return location_offset;
    }

    public TextView getDate() {
        return date;
    }

    public TextView getTime() {
        return time;
    }
}
