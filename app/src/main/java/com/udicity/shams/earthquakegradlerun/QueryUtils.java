package com.udicity.shams.earthquakegradlerun;

import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Helper methods related to requesting and receiving earthquake data from USGS.
 */
public final class QueryUtils {

    private static final String LOG_TAG = QueryUtils.class.getSimpleName();

    /**
     * Create a private constructor because no one should ever create a {@link QueryUtils} object.
     * This class is only meant to hold static variables and methods, which can be accessed
     * directly from the class name QueryUtils (and an object instance of QueryUtils is not needed).
     */
    private QueryUtils() {
    }

    /**
     * Return a list of {@link EarthQuake} objects that has been built up from
     * parsing a JSON response.
     */

    public static List<EarthQuake> fetchEarthQuakeData(String urlString)
    {
        URL url = createUrl(urlString);

        String jsonResponse = null;

        try {
                Thread.sleep(2000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        try {
            jsonResponse = makeHttpRequest(url);
        }catch (IOException e) {
            Log.e(LOG_TAG,"Error return json Response : " ,e);
        }

        List<EarthQuake> earthQuakeArrayList = extractJsonDate(jsonResponse);
        Log.v(LOG_TAG,"Fetch Earth Quake Data");
        return earthQuakeArrayList;
    }

    private static URL createUrl(String url)
    {
        URL mUrl = null;
        try
        {
            mUrl = new URL(url);
        }catch (MalformedURLException e)
        {
            Log.e(LOG_TAG,"error with creating url : ",e);
        }
        return mUrl;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";
        if (url == null)
        {
            return jsonResponse;
        }
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            if (httpURLConnection.getResponseCode() == 200)
            {
                inputStream = httpURLConnection.getInputStream();
                jsonResponse = readFromInputStream(inputStream);
            }else {
                Log.e(LOG_TAG ,"Http Connection Error : " + httpURLConnection.getResponseCode());
            }
        }catch (IOException e){
            Log.e(LOG_TAG,"Error while retrieving json result : " ,e);
        }
        finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }
    private static String readFromInputStream(InputStream inputStream) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line = bufferedReader.readLine();
            while(line != null)
            {
                stringBuilder.append(line);
                line = bufferedReader.readLine();
            }
        }
        return stringBuilder.toString();
    }

    private static List<EarthQuake> extractJsonDate(String jsonResponse){

        List<EarthQuake> arrayList = new ArrayList<>();

        if(TextUtils.isEmpty(jsonResponse)) {
            return null;
        }

        try {
            JSONObject mainJsonObject = new JSONObject(jsonResponse);
            JSONArray jsonArray = mainJsonObject.getJSONArray("features");

            //if (jsonArray.length() > 0) {

            for (int i = 0; i < jsonArray.length(); i++) {
                // JSONObject currentJsonObject = jsonArray.getJSONObject(i);
                JSONObject PropertiesJsonObject = jsonArray.getJSONObject(i).getJSONObject("properties");
                // JSONObject PropertiesJsonObject = currentJsonObject.getJSONObject("properties");
                double mag = PropertiesJsonObject.getDouble("mag");
                String place = PropertiesJsonObject.getString("place");
                Long time = PropertiesJsonObject.getLong("time");
                String url = PropertiesJsonObject.getString("url");

                arrayList.add(new EarthQuake(mag, place, time, url));
            }
      //  }
        }catch (JSONException e){
            Log.e(LOG_TAG,"Error parsing json file : ",e);
        }
        return arrayList;
    }
}