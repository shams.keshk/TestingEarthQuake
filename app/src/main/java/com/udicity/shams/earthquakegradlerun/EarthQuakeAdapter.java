package com.udicity.shams.earthquakegradlerun;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import java.text.SimpleDateFormat;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by shams on 09/07/17.
 */

public class EarthQuakeAdapter extends ArrayAdapter<EarthQuake> {

    private final String LOCATION_SEPARATOR = " of ";
    private Context context;
    private String primary_location ;
    private String location_offset ;
    private String mainLocation;
    private String []placeParts ;
    private String formattedDate;
    private String formattedTime;
    private String magintude;

    public EarthQuakeAdapter(Activity activity, ArrayList<EarthQuake> earthQuakeArrayList,Context context) {
        super(activity, 0, earthQuakeArrayList);
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        EarthQuakeViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.earth_quake_list_items, parent, false);
            viewHolder = new EarthQuakeViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (EarthQuakeViewHolder) convertView.getTag();
        }

        EarthQuake currentEarthQuake = getItem(position);

        assert currentEarthQuake != null;

        Date date = new Date(currentEarthQuake.getDate());

        GradientDrawable magnitudeCircle = (GradientDrawable) viewHolder.getMagnitude().getBackground();

        int magnitudeColor = getMagnitudeColor(currentEarthQuake.getMagnitude());

        magnitudeCircle.setColor(magnitudeColor);

        magintude = decimalFormat(currentEarthQuake.getMagnitude());

        mainLocation = currentEarthQuake.getPlace();

        separateLocation(mainLocation);

        formattedDate = formatDate(date);

        formattedTime = formatTime(date);

        viewHolder.getMagnitude().setText(magintude);

        viewHolder.getPrimary_location().setText(primary_location);

        viewHolder.getLocation_offset().setText(location_offset);

        viewHolder.getDate().setText(formattedDate);

        viewHolder.getTime().setText(formattedTime);

        return convertView;
    }

    public void separateLocation(String location) {
        if(location.contains(LOCATION_SEPARATOR))
        {
            placeParts = location.split(LOCATION_SEPARATOR);
            location_offset = placeParts[0]  + LOCATION_SEPARATOR;
            primary_location = placeParts[1] ;
        }else
        {
            location_offset = context.getString(R.string.near_by);
            primary_location = location;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public String formatDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("LLL dd, yyyy");
        return simpleDateFormat.format(date);
    }

    public String formatTime(Date date) {
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("h:mm a");
        return simpleTimeFormat.format(date);
    }
    public String decimalFormat(double magnitude)
    {
        DecimalFormat decimalFormat = new DecimalFormat("0.0");
        return decimalFormat.format(magnitude);
    }
    public int getMagnitudeColor(double magnitude)
    {
        int magnitudeResourceId;
        int magnitudeFloor = (int) Math.floor(magnitude);
        switch (magnitudeFloor)
        {
            case 0:
            case 1:
                magnitudeResourceId = R.color.magnitude1;
                break;
            case 2:
                magnitudeResourceId = R.color.magnitude2;
                break;
            case 3:
                magnitudeResourceId = R.color.magnitude3;
                break;
            case 4:
                magnitudeResourceId = R.color.magnitude4;
                break;
            case 5:
                magnitudeResourceId = R.color.magnitude5;
                break;
            case 6:
                magnitudeResourceId = R.color.magnitude6;
                break;
            case 7:
                magnitudeResourceId = R.color.magnitude7;
                break;
            case 8:
                magnitudeResourceId = R.color.magnitude8;
                break;
            case 9:
                magnitudeResourceId = R.color.magnitude9;
                break;
            default:
                magnitudeResourceId = R.color.magnitude10plus;
                break;
        }
        return ContextCompat.getColor(getContext() , magnitudeResourceId);
    }

}
